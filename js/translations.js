var translations =
   {
      "et": [
         {
            "left_question"  :  "Kuidas<br>genoomika inimese tervise paremaks muudab?",
            "right_question" :  "DNA<br>järjestamine",
            "watch_video"    :  "Vaata",
            "video_one"      :  "videos/WTB11293_ET.mp4",
            "video_two"      :  "videos/WTB11400_EE.mp4",
         }
      ],
      "en": [
         {
            "left_question"  :  "Envisioning<br>how genomics<br>will improve<br>human health",
            "right_question" :  "DNA<br>sequencing",
            "watch_video"    :  "Play",
            "video_one"      :  "videos/WTB11293_EN.mp4",
            "video_two"      :  "videos/WTB11400_EN.mp4",
         }
      ],
      "ru": [
         {
            "left_question"  :  "Представляя,<br>как геномика<br>улучшит<br>здоровье человека",
            "right_question" :  "Утановление последовательности ДНК",
            "watch_video"    :  "Смотреть",
            "video_one"      :  "videos/WTB11293_RU.mp4",
            "video_two"      :  "videos/WTB11400_RU.mp4",
         }
      // ],
      // "fi": [
      //    {
      //       "left_question"  :  "Visio siitä,<br>miten genomiikka<br>tulee parantamaan<br>ihmisten terveyttä",
      //       "right_question" :  "DNA:n<br>sekvensointi",
      //       "watch_video"    :  "Katso",
      //       "video_one"      :  "videos/WTB11293_FI.mp4",
      //       "video_two"      :  "videos/WTB11400_FI.mp4",
      //    }
      ]
   }